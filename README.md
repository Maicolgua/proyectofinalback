## Proyecto Final Master Desarrollo Web Fullstackt



El proyecto es un sitio web de ecommerce de productos panificados. Donde el usuario para comprar un producto tiene que loguearse. <br />
Contiene una parte autoadministrable, donde el administrador puede crear las categorias y productos. También puede ver los pedidos y editar o borrar los productos.


### `Variabes de entorno`

- DATABASE
- PORT
- JWT_SECRET
- BRAINTREE_MERCHANT_ID
- BRAIN_TREE_PUBLIC_KEY
- API_KEY
- GOOGLE_CLIENT_ID
