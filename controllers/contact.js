const Info = require("../models/info");

const { errorHandler } = require("../helpers/dbErrorHandler");

exports.contact = (req, res) => {
  // console.log("req.body", req.body);
  const info = new Info(req.body);
  info.save((err, info) => {
    if (err) {
      return res.status(400).json({
        // error: errorHandler(err)
        error: "Error, intentar nuevamente",
      });
    }

    res.json({
      info,
    });
  });
};
