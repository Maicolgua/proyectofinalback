exports.userSignupValidator = (req, res, next) => {
  req.check("name", "Este campo es obligatorio").notEmpty();
  req
    .check("email", "Email debe tener entre 4 to 32 caracteres")
    .matches(/.+\@.+\..+/)
    .withMessage("Email debe contener @")
    .isLength({
      min: 4,
      max: 32,
    });
  req.check("password", "Este campo es obligatorio").notEmpty();
  req
    .check("password")
    .isLength({ min: 6 })
    .withMessage("La contraseña debe tener al menos 6 caracteres");

  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map((error) => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};
