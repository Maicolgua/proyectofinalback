const express = require("express");
const router = express.Router();

const {
  signup,
  signin,
  signout,
  requireSignin,
  googleLogin,
  facebookLogin,
} = require("../controllers/auth");
const { userSignupValidator } = require("../validator");

router.post("/signup", userSignupValidator, signup);
router.post("/signin", signin);
router.get("/signout", signout);

//google Route
router.post("/google-login", googleLogin);
router.post("/facebook-login", facebookLogin);

module.exports = router;
